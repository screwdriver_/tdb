# TDb

TDb is a simple NoSQL relational database.



## TDbLang

TDbLang is a simple request language used by this database.
In this document, this language is described in a Backus-Naur Form (BNF).

The language has the following keywords:

| Keyword  | Description                                                                |
|----------|----------------------------------------------------------------------------|
| let      | Declares a variable                                                        |
| if       | Declares a condition                                                       |
| else     | Declares a else block                                                      |
| while    | Declares a loop                                                            |
| for      | Declares a for loop                                                        |
| break    | Stops the loop                                                             |
| continue | Skips the remaining instructions of the loop and goes back to the beginnig |
| func     | Declares a function                                                        |
| return   | Returns a value from a function                                            |



### Variables

A variables allows to allocate memory space on the stack, to store data in it and to associate a name to this data.
It can be declared as follows:

```
"let" type[size] name ["=" expression] 
```

Here, ``type`` is the data type of the variable, ``size`` is the size of the variable (if not defined, the variable is assumed to be the default size of the type), ``name`` is the name of the variable and ``expression`` is an expression that will be assigned to the variable.

If the variable isn't defined at declaration, its value will be ``0``.

After being declared, a variable can be defined using the following declaration:

```
name = expression
```

If the result of an expression if too large or too small to fit into a variable, it's automaticly cast.



### Condition

Conditions allow to perform instructions only if a specific condition is fullfilled.
It can be declared as follows:

```
CONDITION :=	"if" expression
				"{"
					instructions
				"}"
				["else" [CONDITION]
				"{"
					instructions
				"}"]
```

Here, ``expression`` is an expression that will be evaluated to check to the condition and ``instructions`` is a set of instructions that will be performed if the condition is fullfilled (or not).

The instructions in the ``if`` block are performed if the condition is fulfilled. Although, if a ``else`` block is declared and the condition isn't fulfilled, then its content will be performed instead.

If one or several ``else if`` blocks are declared, the first whose condition is fulfilled will be performed.



### Loops

A loop is a block that is performed until its condition is no longer fulfilled.
A ``while`` loop can be declared as follows:

```
"while" expression
"{"
	instructions
"}"
```

Here, ``expression`` is a condition that will be check every time the program enters the loop and ``instructions`` is a set of instructions that will be performed while the condition is fulfilled.

Every time the program enters a loop, it checks its condition. If the condition isn't fulfilled, the program skips the while.
If the program entered the loop, when it reaches its end, it will go back to the beginning if the condition is still fulfilled.

``for`` loops allow to perform instructions in a range of indexes.
A ``for`` loop can be declared as follows:

```
"for" name begin end step
"{"
	instructions
"}"
```

Here, ``name`` is the name of the index, ``begin`` is the beginning index, ``end`` is the ending index and ``step`` is the step between the index of each loop.

The loop will be performed while the index is lower than ``end``. ``step`` is added to the index at every loop.
The index can be accessed using a variable named according to ``name``.

The ``break`` instruction can be placed inside of a loop to stop it. The ``continue`` instruction can also be placed to skip the remaining instructions and go back to the loop's entry.



### Functions

Functions allow to create named blocks of instruction that can be called from everywhere in the code. When a function is returned, the program continues at the instruction after the one that called the function.
A function can be declared as follows:

```
ARGUMENTS :=	type[size] name["," ARGUMENTS]

"func" [type[size]] name["|"ARGUMENTS"|"]
"{"
	instructions
"}"
```

A function can take arguments and can return a value.
A value can be returned using the following declaration:

```
"return" expression
```



### Expression

Expressions allows to perform operations, comparisons, function calls, variable assignements, array accesses, etc...

The following operators can be used in expressions:

| Operator | Priority | Description                |
|----------|----------|----------------------------|
| +        | 4        | Addition                   |
| -        | 4        | Subtraction                |
| \*       | 2        | Multiplication             |
| /        | 2        | Division                   |
| %        | 2        | Modulo                     |
| \*\*     | 3        | Power                      |
| ++       | 1        | Incrementation             |
| --       | 1        | Decrementation             |
| =        | 5        | Assignement                |
| +=       | 5        | Addition assignement       |
| -=       | 5        | Subtraction assignement    |
| \*=      | 5        | Multiplication assignement |
| /=       | 5        | Division assignement       |
| %=       | 5        | Modulo assignement         |
| \*\*=    | 5        | Power assignement          |
| ==       | 6        | Comparison                 |
| !=       | 6        | Comparison                 |
| <        | 6        | Comparison                 |
| <=       | 6        | Comparison                 |
| >        | 6        | Comparison                 |
| >=       | 6        | Comparison                 |
| &        | 7        | AND bitwise                |
| |        | 7        | OR bitwise                 |
| ^        | 7        | XOR bitwise                |
| ()       | 0        | Function call              |
| []       | 0        | Array access               |



## Errors

| # | Name               | Description                     |
|---|--------------------|---------------------------------|
| 0 | UNKNOWN            | Unknown error!                  |
| 1 | TOO\_LONG          | Request too long!               |
| 2 | BAD\_REQUEST       | Ill-formed request!             |
| 3 | FORBIDDEN          | Permission denied!              |
| 4 | TABLE\_NOT\_FOUND  | Table not found!                |
| 5 | COLUMN\_NOT\_FOUND | Column not found!               |
| 6 | IO                 | I/O operation error!            |



## Protocol

The database protocol is designed to efficiently transmit data.
All the values described in this document for this protocol are not ASCII but decimal values, except the ones that start with a backslash.

This protocol has several different message types.

| # | Name            | Description                                   |
|---|-----------------|-----------------------------------------------|
| 0 | login\_response | Tells the client if it successfully logged in |
| 1 | data            | Sends back data after request                 |
| 2 | error           | Tells the client an error happened            |

The format of each messages are the following:



### Login response

| Name    | Size (bytes) | Description/Value                   |
|---------|--------------|-------------------------------------|
| type    | 1            | ``0``                               |
| success | 1            | ``0`` if failed, ``1`` if succeeded |



### Data

| Name          | Size (bytes)  | Description/Value                                                                                                             |
|---------------|---------------|-------------------------------------------------------------------------------------------------------------------------------|
| type          | 1             | ``1``                                                                                                                         |
| head\_size    | 4             | The number of columns that were returned                                                                                      |
| head\_content |               | The list of all the columns. The message contains for each: the name ended by ``\0``, the type (1 byte) and the size (1 byte) |
| lines         |               | All the lines, beginning with the index (4 bytes), followed by the data (in the order of the columns)                         |
| end           | a line's size | A line's size filled with ``\0``                                                                                              |



### Error

| Name    | Size (bytes) | Description/Value                 |
|---------|--------------|-----------------------------------|
| type    | 1            | ``2``                             |
| code    | 1            | The error code                    |
| message |              | The error message ended by ``\0`` |
