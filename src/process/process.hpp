#ifndef PROCESS_HPP
# define PROCESS_HPP

# include <string>

# include <tungsten.hpp>

namespace TDb
{
	using namespace std;
	using namespace Tungsten;

	Program compile(const string& str);
}

#endif
