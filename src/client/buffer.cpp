#include "client.hpp"

using namespace TDb;

void Buffer::read(const sock_t sock)
{
	while(sock_is_ready(sock.fd)) {
		const auto s = BUFFER_SIZE - size;

		if(s == 0) {
			reset();
			// TODO Error: Buffer overflow
		}

		size += sock_read(sock.fd, data + size, s);
	}
}

bool Buffer::get(string& buff, const size_t length)
{
	if(available() >= length) {
		buff.resize(length);
		memcpy(buff.data(), data + i, length);
		i += length;

		return true;
	}

	i = 0;
	return false;
}

bool Buffer::get_string(string& buff)
{
	size_t length = 0;
	while(data[i + length] && i + length < BUFFER_SIZE) ++length;

	if(i + length >= BUFFER_SIZE) {
		i = 0;
		return false;
	}

	buff.resize(length);
	memcpy(buff.data(), data + i, length);
	i += length;

	return true;
}
