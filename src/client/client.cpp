#include "client.hpp"
#include "../tdb.hpp"

using namespace TDb;

Client::Client(Database& database, const sock_t sock)
	: database{&database}, sock{sock}
{
	database.log(string("New connection from `") + sock.host + '`');
}

Client::~Client()
{
	database->log(string("`") + sock.host + "` disconnected");
}

void Client::handle()
{
	buff.read(sock);

	RequestType type;
	if(!buff.get(type)) return;

	if(type == LOGIN) {
		handle_login();
		return;
	}

	if(!is_logged()) {
		throw error_exception(FORBIDDEN);
	}

	switch(type) {
		case GET: {
			handle_get();
			break;
		}

		case SET: {
			handle_set();
			break;
		}

		case INSERT: {
			handle_insert();
			break;
		}

		case REMOVE: {
			handle_remove();
			break;
		}

		case CLEAR: {
			handle_clear();
			break;
		}

		case PROCESS: {
			handle_process();
			break;
		}

		default: return;
	}
}

void Client::handle_login()
{
	string user;
	if(!buff.get_string(user)) return;

	string pass;
	if(!buff.get_string(pass)) return;

	const auto success = login(user, pass);
	write_value<uint8_t>(0);
	sock_write(sock.fd, (const char*) &success, 1);

	buff.reset();
}

void Client::handle_get()
{
	Table* table;
	if(!get_table(table)) return;

	string condition;
	if(!buff.get_string(condition)) return;

	const auto row_size = table->get_row_size();
	Condition c(condition);

	send_data_head(table->get_columns());
	table->get_rows(c, [this, &row_size](const Row& row)
		{
			sock_write(sock.fd, (const char*)&row.index, sizeof(row.index));
			sock_write(sock.fd, row.data, row_size);
		});

	send_data_end(row_size);
	buff.reset();
}

void Client::handle_set()
{
	Table* table;
	if(!get_table(table)) return;

	string condition;
	if(!buff.get_string(condition)) return;

	// TODO

	buff.reset();
}

void Client::handle_insert()
{
	Table* table;
	if(!get_table(table)) return;

	// TODO

	buff.reset();
}

void Client::handle_remove()
{
	Table* table;
	if(!get_table(table)) return;

	string condition;
	if(!buff.get_string(condition)) return;

	// TODO

	buff.reset();
}

void Client::handle_clear()
{
	Table* table;
	if(!get_table(table)) return;

	table->clear();

	buff.reset();
}

void Client::handle_process()
{
	// TODO

	buff.reset();
}

void Client::send_error(const error_exception& error)
{
	write_value<uint8_t>(2);
	write_value(error.get_error());

	const auto message = error.what();
	sock_write(sock.fd, message, strlen(message) + 1);
}

bool Client::get_table(Table*& table)
{
	string table_name;
	if(!buff.get_string(table_name)) return false;

	const auto t = database->get_table(table_name);
	if(!t) throw error_exception(TABLE_NOT_FOUND);

	table = t;
	return true;
}

bool Client::login(const string& user, const string& pass)
{
	if(user.empty() || pass.empty()) return false;

	const auto u = database->get_user(user);
	if(!u) return false;

	if(u->verify_password(pass)) {
		this->user = u;
		return true;
	}

	return false;
}

void Client::send_data_head(const vector<Column>& columns)
{
	write_value<uint8_t>(1);
	write_value(columns.size());

	for(const auto& c : columns) {
		const auto& name = c.get_name();

		sock_write(sock.fd, name.c_str(), name.size() + 1);
		write_value(c.get_type());
		write_value(c.get_size());
	}
}

void Client::send_data_end(const size_t size)
{
	const char* empty_buffer = (const char*) calloc(1, size);
	sock_write(sock.fd, empty_buffer, size);
	free((void*) empty_buffer);
}
