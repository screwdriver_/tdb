#include "command.hpp"
#include "../tdb.hpp"

using namespace TDb;

void t_list(Database& database)
{
	const auto& tables = database.get_tables();
	cout << "Tables list (" << tables.size() << " elements):" << '\n';

	for(const auto& t : tables) {
		cout << "- " << t.get_name() << '\n';
	}
}

void t_create(Database& database, const vector<string>& args)
{
	if(args.size() != 2) {
		invalid_usage();
		return;
	}

	const auto& name = args[1];
	cout << "Table `" << name;

	if(database.create_table(name)) {
		cout << "` created!";
	} else {
		cout << "` already exists!";
	}

	cout << '\n';
}

void t_rename(Database& database, const vector<string>& args)
{
	if(args.size() != 3) {
		invalid_usage();
		return;
	}

	const auto table = database.get_table(args[1]);

	if(database.get_table(args[2])) {
		cout << "Name `" << args[2] << "` already used!" << '\n';
		return;
	}

	cout << "Table `" << args[1];

	if(table) {
		// TODO
		cout << "` renamed into `" << args[2] << "`!";
	} else {
		cout << "` not found!";
	}
}

void t_remove(Database& database, const vector<string>& args)
{
	if(args.size() != 2) {
		invalid_usage();
		return;
	}

	const auto& name = args[1];
	cout << "Table `" << name;

	if(database.remove_table(name)) {
		cout << "` removed!";
	} else {
		cout << "` not found!";
	}

	cout << '\n';
}

void TablesCommand::perform(Database& database,
	const vector<string>& args) const
{
	if(args.empty()) {
		t_list(database);
		return;
	}

	const auto& action = args.front();

	if(action == "list") {
		t_list(database);
	} else if(action == "create") {
		t_create(database, args);
	} else if(action == "rename") {
		t_rename(database, args);
	} else if(action == "remove") {
		t_remove(database, args);
	} else {
		invalid_usage();
	}
}
