#ifndef COMMAND_HPP
# define COMMAND_HPP

# include<iostream>
# include<string>
# include<vector>

namespace TDb
{
	using namespace std;

	inline void invalid_usage()
	{
		cout << "Invalid usage! Type \"help\" to show help!" << '\n';
	}

	class Database;

	class Command
	{
		public:
			inline Command(const string&& name,
				const string&& usage,
				const string&& description)
				: name{name}, usage{usage}, description{description}
			{}

			inline const string& get_name() const
			{
				return name;
			}

			inline const string& get_usage() const
			{
				return usage;
			}

			inline const string& get_description() const
			{
				return description;
			}

			virtual void perform(Database& database,
				const vector<string>& args) const = 0;

		private:
			const string name;
			const string usage;
			const string description;
	};

	class HelpCommand : public Command
	{
		public:
			inline HelpCommand()
				: Command("help", "help", "Shows commands list")
			{}

			void perform(Database& database,
				const vector<string>&) const override;
	};

	class UsersCommand : public Command
	{
		public:
			inline UsersCommand()
				: Command("users",
				"users [list | (create <name> <password>) | (rename <user> <new_name>) | (setpassword <user> <new_password>) | (remove <user>)]",
				"Handling the database's users")
			{}

			void perform(Database& database,
				const vector<string>& args) const override;
	};

	class TablesCommand : public Command
	{
		public:
			inline TablesCommand()
				: Command("tables",
				"tables [list | (create <name>) | (rename <table> <new_name>) | (remove <table>)]",
				"Handling the database's tables")
			{}

			void perform(Database& database,
				const vector<string>& args) const override;
	};

	class ColumnsCommand : public Command
	{
		public:
			inline ColumnsCommand()
				: Command("columns",
				"columns <table> [list | (set <name> <type> [size]) | (rename <column> <new_name>) | (remove <column>)]",
				"Handling a tables's columns")
			{}

			void perform(Database& database,
				const vector<string>& args) const override;
	};

	class ExitCommand : public Command
	{
		public:
			inline ExitCommand()
				: Command("exit", "exit", "Stops the server")
			{}

			void perform(Database& database,
				const vector<string>&) const override;
	};
}

#endif
