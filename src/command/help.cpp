#include "command.hpp"
#include "../tdb.hpp"

using namespace TDb;

void HelpCommand::perform(Database& database, const vector<string>&) const
{
	cout << "----------" << '\n';
	cout << "   Help   " << '\n';
	cout << "----------" << '\n';
	cout << '\n';
	cout << "Commands list:" << '\n';

	for(const auto& c : database.get_commands()) {
		cout << c->get_usage() << ": " << c->get_description() << '\n';
	}
}
