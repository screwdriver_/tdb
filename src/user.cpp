#include "tdb.hpp"

using namespace TDb;

User::User(const string& name, const string& password)
	: name{name}
{
	this->salt = random_string(SALT_LENGTH);
	this->password = hash(password + salt);

	save();
}

User::User(const string& file)
{
	ifstream stream(file);

	if(!stream.is_open()) {
		throw runtime_error("User file not found!");
	}

	stream.seekg(0, ios::end);
	const auto length = stream.tellg();
	stream.seekg(0);

	string buffer;
	buffer.reserve(length);
	stream.read(&buffer[0], length);

	const auto json = parse(buffer);

	if(!json->is_object()) {
		throw invalid_argument("Invalid user file!");
	}

	const auto& o = *((Object*) json.get());

	try {
		this->name = (const char*) *((Value*) o["name"].get());
		this->password = (const char*) *((Value*) o["password"].get());
		this->salt = (const char*) *((Value*) o["salt"].get());
	} catch(const invalid_argument& e) {
		throw invalid_argument("Invalid user file!");
	}
}

void User::save()
{
	Object json;
	json["name"] = Value(name);
	json["password"] = Value(password);
	json["salt"] = Value(salt);

	ofstream stream(string(USERS_DIR) + name + ".user");
	stream << json.dump(true);
	stream.flush();
}

void User::remove_user()
{
	const string path(string(USERS_DIR) + name + ".user");
	remove(path.c_str());
}
