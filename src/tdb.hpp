#ifndef TDB_HPP
# define TDB_HPP

# include<array>
# include<climits>
# include<exception>
# include<fstream>
# include<functional>
# include<iostream>
# include<memory>
# include<sstream>
# include<string>
# include<unordered_map>
# include<vector>

# include<errno.h>
# include<dirent.h>
# include<sys/stat.h>

# include<tengine.hpp>
# include<json.hpp>

# include "command/command.hpp"
# include "client/client.hpp"
# include "process/process.hpp"

# define LOGS_DIR		"/var/log/TDb/"
# define ETC_DIR		"/etc/TDb/"
# define CONFIG_FILE	"/etc/TDb/config.json"
# define USERS_DIR		"/etc/TDb/users/"
# define TABLES_DIR		"/etc/TDb/tables/"

# define SALT_LENGTH	8
# define DEFAULT_PORT	8123

namespace TDb
{
	using namespace std;
	using namespace TEngine;
	using namespace JSON;

	string get_date();
	string get_log_time();

	bool make_dir(const char* path);
	void foreach_file(const char* dir_path,
		const function<void(const string&)> handle);

	string random_string(const size_t length);
	string hash(const string input);

	vector<string> str_split(const string& str, const char sp);

	enum Error : uint8_t
	{
		UNKNOWN = 0,
		TOO_LONG = 1,
		BAD_REQUEST = 2,
		FORBIDDEN = 3,
		TABLE_NOT_FOUND = 4,
		COLUMN_NOT_FOUND = 5,
		IO = 6
	};

	const unordered_map<Error, const char*> errors = {
		{UNKNOWN, "Unknown error!"},
		{TOO_LONG, "Request too long!"},
		{BAD_REQUEST, "Ill-formed request!"},
		{FORBIDDEN, "Permission denied!"},
		{TABLE_NOT_FOUND, "Table not found!"},
		{COLUMN_NOT_FOUND, "Column not found!"},
		{IO, "I/O operation error!"}
	};

	const char* get_error_str(const Error error);

	class error_exception : public exception
	{
		public:
			inline error_exception(const Error error)
				: error{error}
			{}

			inline Error get_error() const noexcept
			{
				return error;
			}

			inline virtual const char* what() const noexcept
			{
				return get_error_str(error);
			}

		private:
			const Error error;
	};

	enum RequestType : uint8_t
	{
		LOGIN = 0,
		GET,
		SET,
		INSERT,
		REMOVE,
		CLEAR,
		PROCESS
	};

	class User
	{
		public:
			User(const string& name, const string& password);
			User(const string& file);

			inline const string& get_name() const
			{
				return name;
			}

			inline void set_name(const string& name)
			{
				this->name = name;
			}

			inline bool verify_password(const string& password) const
			{
				return (this->password == hash(password + salt));
			}

			inline void set_password(const string& password, const bool hashed)
			{
				this->password = (hashed ? password : hash(password + salt));
			}
			
			inline const string& get_salt() const
			{
				return salt;
			}

			void save();
			void remove_user();

		private:
			string name;
			string password;
			string salt;
	};

	class Database
	{
		public:
			void init();
			void init_logs();
			void read_config();
			void init_users();
			void init_tables();
			void init_commands();
			void init_processes();
			void init_clients();

			void log(const string&& log);

			bool create_user(const string& name, const string& password);
			bool remove_user(const string& name);

			inline vector<User>& get_users()
			{
				return users;
			}

			User* get_user(const string& name);

			bool create_table(const string& name);
			bool remove_table(const string& name);

			inline vector<Table>& get_tables()
			{
				return tables;
			}

			Table* get_table(const string& name);

			template<typename C>
			inline void register_command()
			{
				commands.emplace_back(new C);
			}

			inline const vector<unique_ptr<Command>>& get_commands() const
			{
				return commands;
			}

			const Command* get_command(const string& name) const;
			bool exec_command(const string& command);

		private:
			ofstream logs;

			port_t port = DEFAULT_PORT;

			vector<User> users;

			vector<Table> tables;

			vector<unique_ptr<Command>> commands;

			unique_ptr<ClientsManager> clients_manager;

			void create_config();

			vector<User>::iterator get_user_iter(const string& name);
			vector<Table>::iterator get_table_iter(const string& name);
	};
}

#endif
