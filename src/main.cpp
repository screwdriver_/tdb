#include "tdb.hpp"

using namespace std;

int main()
{
	using namespace TDb;

	Database database;
	database.init();
	database.init_logs();
	database.read_config();
	database.init_users();
	database.init_tables();
	database.init_commands();
	database.init_processes();
	database.init_clients();

	while(true) {
		string line;
		getline(cin, line);

		if(!database.exec_command(line)) {
			cout << "Unknown command! Type \"help\" to show commands list." << '\n';
		}
	}

	return 0;
}
