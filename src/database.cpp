#include "tdb.hpp"

using namespace TDb;

void Database::init()
{
	log("Initializing database...");

	mkdir(ETC_DIR, 0777);
}

void Database::init_logs()
{
	mkdir(LOGS_DIR, 0777);
	errno = 0;

	logs = ofstream(string(LOGS_DIR) + get_date() + ".log", ofstream::app);
}

void Database::read_config()
{
	ifstream stream(CONFIG_FILE);

	if(!stream.is_open()) {
		create_config();
		return;
	}

	log("Reading config...");

	stream.seekg(0, ios::end);
	const auto length = stream.tellg();
	stream.seekg(0);

	string buffer;
	buffer.resize(length);
	stream.read(&buffer[0], length);

	stream.close();

	const auto json = parse(buffer);

	if(!json->is_object()) {
		throw invalid_argument("Invalid configuration file!");
	}

	const auto obj = *((Object*) json.get());
	const auto p = obj["port"];

	if(!p->is_value()) {
		throw invalid_argument("Invalid configuration file!");
	}

	port = *((Value*) p.get());
}

void Database::create_config()
{
	log("Creating config file...");

	Object json;
	json["port"] = Value(DEFAULT_PORT);

	ofstream stream(CONFIG_FILE);
	stream << json.dump(true);
	stream.flush();
}

void Database::init_users()
{
	log("Initializing users...");

	srand(time(0));

	if(make_dir(USERS_DIR)) return;
	foreach_file(USERS_DIR, [this](const string& name)
		{
			users.emplace_back(string(USERS_DIR) + name);
		});
}

void Database::init_tables()
{
	log("Initializing tables...");

	if(make_dir(TABLES_DIR)) return;
	foreach_file(TABLES_DIR, [this](const string& name)
		{
			tables.emplace_back(TABLES_DIR, name);
		});
}

void Database::init_commands()
{
	log("Initializing default commands...");

	register_command<HelpCommand>();
	register_command<UsersCommand>();
	register_command<TablesCommand>();
	register_command<ColumnsCommand>();
	register_command<ExitCommand>();
}

void Database::init_processes()
{
	log("Initializing processes...");

	// TODO
}

void Database::init_clients()
{
	log("Initializing clients listener...");
	clients_manager = unique_ptr<ClientsManager>(new ClientsManager(*this,
		port));
}

void Database::log(const string&& log)
{
	const auto time = get_log_time();

	cout << '[' << time << "] " << log << '\n';
	logs << '[' << time << "] " << log << '\n';
	logs.flush();
}

bool Database::create_user(const string& name, const string& password)
{
	if(get_user(name)) return false;

	users.emplace_back(name, password);
	return true;
}

bool Database::remove_user(const string& name)
{
	const auto& user = get_user_iter(name);
	if(user == users.cend()) return false;

	user->remove_user();
	users.erase(user);
	return true;
}

vector<User>::iterator Database::get_user_iter(const string& name)
{
	for(auto i = users.begin(); i != users.end(); ++i) {
		if(i->get_name() == name) {
			return i;
		}
	}

	return users.end();
}

User* Database::get_user(const string& name)
{
	const auto user = get_user_iter(name);
	return (user == users.cend() ? nullptr : &(*user));
}

bool Database::create_table(const string& name)
{
	if(get_table(name)) return false;

	tables.emplace_back(TABLES_DIR, name);
	return true;
}

bool Database::remove_table(const string& name)
{
	const auto& table = get_table_iter(name);
	if(table == tables.cend()) return false;

	table->remove_table();
	tables.erase(table);
	return true;
}

vector<Table>::iterator Database::get_table_iter(const string& name)
{
	for(auto i = tables.begin(); i != tables.end(); ++i) {
		if(i->get_name() == name) {
			return i;
		}
	}

	return tables.end();
}

Table* Database::get_table(const string& name)
{
	const auto table = get_table_iter(name);
	return (table == tables.cend() ? nullptr : &(*table));
}

const Command* Database::get_command(const string& name) const
{
	for(const auto& c : commands) {
		if(c->get_name() == name) {
			return c.get();
		}
	}

	return nullptr;
}

bool Database::exec_command(const string& command)
{
	const auto split = str_split(command, ' ');
	if(split.empty()) return false;

	const auto c = get_command(split.front());
	if(!c) return false;

	const vector<string> args(split.cbegin() + 1, split.cend());
	c->perform(*this, args);

	return true;
}
